module meklib.utils.argparser;
import meklib.utils.array.container;
import meklib.utils.traits;
import meklib.utils.typedmem;
import std.parallelism;
import std.functional;
import std.exception : enforce;
import std.container : Array;
import std.algorithm : map;
import std.traits	 : isDelegate;
import std.array	 : array;
import std.conv		 : to;

/**
 * Alias to the accepted delegate callback type.
 */
private alias DelegateCallback = void delegate(string, ArgumentType, string[]);

/**
 * Returns whatever the given callable is convertible to a callback.
 */
private enum bool isCallback(C) = callableDescribes!(C, void, string, ArgumentType, string[]);

/**
 * Converts a callable to a calllback delegate.
 */
private DelegateCallback toCallback(C)(C candidate)
{
	static if(is(C : DelegateCallback))
		return candidate;
	else static if(isCallback!C)
		return candidate.toDelegate().toCallback();
	else
		static assert(0, "\"" ~ typeof(candidate).stringof ~ "\" is not a convertible to a \"" ~
			DelegateCallback.stringof ~ "\" callback.");
}


/// Action on a unknow trigger
public enum UnknowTriggerAction : ubyte { None, Throw, AsUnused, AsOption }

/// The type of a parsed argument.
public enum ArgumentType : ubyte { Short, Long, Option }


/+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 + Getter Functions
 +/
/**
 * Builds and returns an argument parser.
 */
public ArgParser getParser(C)(C helpCallable)
{
	return ArgParser(toCallback(helpCallable));
}


/// ditto
public ArgParser getParser()
{
	return ArgParser(null);
}


/**
 * A parsing utility. Given a set of callbacks associated with triggers, raises them when they are
 * found in a given sequence.
 */
public struct ArgParser
{
	private enum shortHelp = 'h';
	private enum longHelp = "help";


	// Holds the contaienr with it's sender.
	private struct Holder
	{
		ReadOnlyContainer* _container;
		ArgumentType _senderType;
		string _sender;


		@disable this();

		public this(string sender, const ArgumentType senderType, ReadOnlyContainer* container)
		{
			_container = container;
			_senderType = senderType;
			_sender = sender;
		}

		public this(char shortSender, ReadOnlyContainer* container)
		{
			_container = container;
			_senderType = ArgumentType.Short;
			_sender = to!string(shortSender);
		}

		public this(string longSender, ReadOnlyContainer* container)
		{
			_container = container;
			_senderType = ArgumentType.Long;
			_sender = longSender;
		}


		public @property ReadOnlyContainer*		 container()  nothrow pure @nogc @safe { return _container; }
		public @property immutable(ArgumentType) senderType() const nothrow pure @nogc @safe { return _senderType; }
		public @property immutable(string)		 sender()	  const nothrow pure @nogc @safe { return _sender; }
	}


	// Holds the callback with its relative metadata.
	private struct ReadOnlyContainer
	{
		// Constants.
		public enum size_t ALL_OPTIONS = size_t.max;
		public enum char   NO_SHORT_TRIGGER = char.init;
		public enum string NO_LONG_TRIGGER = string.init;

		// The content fields.
		private DelegateCallback _callback;
		private size_t			 _optionsCount;
		private bool			 _raiseAsync;
		private char			 _shortTrigger;
		private string			 _longTrigger;
		private string			 _description;


		public this(C)(C callback, immutable size_t optionsCount, immutable char shortTrigger,
			immutable string longTrigger, immutable bool raiseAsync)
		{
			_callback = toCallback(callback);
			_optionsCount = optionsCount;
			_raiseAsync = raiseAsync;
			_shortTrigger = shortTrigger;
			_longTrigger = longTrigger;
			_description = string.init;
		}


		public this(C)(C callback, immutable size_t optionsCount, immutable char shortTrigger,
			immutable string longTrigger, immutable bool raiseAsync, immutable string description)
		{
			_callback = toCallback(callback);
			_optionsCount = optionsCount;
			_raiseAsync = raiseAsync;
			_shortTrigger = shortTrigger;
			_longTrigger = longTrigger;
			_description = description;
		}


		// Getters for the internal contents.
		public @property immutable(char)  shortTrigger() const nothrow pure @nogc @safe { return _shortTrigger; }
		public @property immutable(string) longTrigger() const nothrow pure @nogc @safe { return _longTrigger; }
		public @property immutable(string) description() const nothrow pure @nogc @safe { return _description; }


		/**
		 * Invokes the current container callback with the given options,
		 * returning the number of options that were not required by the container.
		 * Params:
		 *		triggerOptions:	the options to invoke the containere with.
		 */
		public const(size_t) invokeWith(const string sender, ArgumentType senderType,
			const string[] triggerOptions)
		{
			/*
			 * Checking either if the callback accepts an unlimited
			 * number of options and there are enough options for the callback.
			 */
			if(_optionsCount > triggerOptions.length)
			{
				// Use all the available options.
				invoke(sender, senderType, triggerOptions);
				return triggerOptions.length;
			}
			else
			{
				// Passes only the strictly necessary options.
				invoke(sender, senderType, triggerOptions[0.._optionsCount]);
				return _optionsCount;
			}
		}


		/**
		 * Internally invokes the callback with the given options, asyncronously if required.
		 */
		private void invoke(const string sender, ArgumentType senderType, const string[] options)
		{
			// Raises the callback asyncronously if required.
			if(_raiseAsync)
				taskPool.put(task(_callback, sender, senderType, options.dup));
			else
				_callback(sender, senderType, options.dup);
		}
	}


	/+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	 + Constructor and Destructor.
	 +/
	@disable this();

	private this(DelegateCallback helpCallable)
	{
		_helpCallable = helpCallable;
	}

	~this()
	{
		clearCallbacks();
	}


	/+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	 + Internal Fields.
	 +/
	// Holds the callbacks and the associated triggers.
	private Array!(ReadOnlyContainer*) _containers;
	// Holds the callback to invoke for help.
	private DelegateCallback _helpCallable = null;

	/+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	 + Add callbacks with relative triggers to the internal collection.
	 +/
	/**
	 * Adds a new callback and the relative triggers to the internal collection.
	 */
	template addCallback(C)
	if(isCallback!C)
	{
		public void addCallback(C callback, const size_t optionsCount, const char shortTrigger,
			const string longTrigger, const bool raiseAsync)
		{
			enforceTrigger(shortTrigger);
			enforceTrigger(longTrigger);
			_containers ~= TypedMem.malloc!ReadOnlyContainer(callback, optionsCount, shortTrigger,
				longTrigger, raiseAsync);
		}

		public void addCallback(C callback, const size_t optionsCount, const char shortTrigger,
			const bool raiseAsync)
		{
			enforceTrigger(shortTrigger);
			_containers ~= TypedMem.malloc!ReadOnlyContainer(callback, optionsCount, shortTrigger,
				ReadOnlyContainer.NO_LONG_TRIGGER, raiseAsync);
		}

		public void addCallback(C callback, const size_t optionsCount, const string longTrigger,
			const bool raiseAsync)
		{
			enforceTrigger(longTrigger);
			_containers ~= TypedMem.malloc!ReadOnlyContainer(callback, optionsCount,
				ReadOnlyContainer.NO_SHORT_TRIGGER, longTrigger, raiseAsync);
		}

		public void addCallback(C callback, const char shortTrigger, const string longTrigger,
			const bool raiseAsync)
		{
			enforceTrigger(shortTrigger);
			enforceTrigger(longTrigger);
			_containers ~= TypedMem.malloc!ReadOnlyContainer(callback, ReadOnlyContainer.ALL_OPTIONS,
				shortTrigger, longTrigger, raiseAsync);
		}

		public void addCallback(C callback, const char shortTrigger, const bool raiseAsync)
		{
			enforceTrigger(shortTrigger);
			_containers ~= TypedMem.malloc!ReadOnlyContainer(callback, ReadOnlyContainer.ALL_OPTIONS,
				shortTrigger, ReadOnlyContainer.NO_LONG_TRIGGER, raiseAsync);
		}

		public void addCallback(C callback, const string longTrigger, const bool raiseAsync)
		{
			enforceTrigger(longTrigger);
			_containers ~= TypedMem.malloc!ReadOnlyContainer(callback, ReadOnlyContainer.ALL_OPTIONS,
				ReadOnlyContainer.NO_SHORT_TRIGGER,	longTrigger, raiseAsync);
		}
	}

	/+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	 + Methods to inspect the internal collection contents.
	 +/
	/**
	 * Params:
	 *		shortTrigger: the short trigger to search for.
	 *		longTrigger: the long trigger to search for.
	 *
	 * Returns: the presense of a callback associated with both triggers inside the collection.
	 */
	public pragma(inline, true) bool containsCallback(const char shortTrigger, const string longTrigger) nothrow @nogc
	{
		return _containers.contains!((c) => c.shortTrigger == shortTrigger
			&& c.longTrigger == longTrigger)();
	}

	/**
	 * Params:
	 *		shortTrigger: the short trigger to search for.
	 *
	 * Returns: the presense of a callback associated with both triggers inside the collection.
	 */
	public pragma(inline, true) bool containsCallback(const char shortTrigger) nothrow @nogc
	{
		return !locateByTrigger(shortTrigger).isNull;
	}

	/**
	 * Params:
	 *		longTrigger: the long trigger to search for.
	 *
	 * Returns: the presense of a callback associated with both triggers inside the collection.
	 */
	public pragma(inline, true) bool containsCallback(const string longTrigger) nothrow @nogc
	{
		return !locateByTrigger(longTrigger).isNull;
	}



	/**
	 * Removes the callcback with the given trigger.
	 */
	public bool removeCallback(const char shortTrigger)
	{
		return removeImpl(_containers.indexOf!("a.shortTrigger == b")(shortTrigger));
	}

	/// ditto
	public bool removeCallback(const string longTrigger)
	{
		return removeImpl(_containers.indexOf!("a.longTrigger == b")(longTrigger));
	}

	/// ditto
	public bool removeCallback(const char shortTrigger, const string longTrigger)
	{
		return removeImpl(_containers.indexOf!((c) => c.shortTrigger == shortTrigger
			&& c.longTrigger == longTrigger)());
	}


	private bool removeImpl(Index)(Index index)
	{
		if(index.isNull)
			return false;
		else
		{
			TypedMem.free(_containers[index.get()]);
			_containers.removeAt(index.get());
			return true;
		}
	}


	/**
	 * Removes all the callbacks from the internal collection.
	 */
	public void clearCallbacks() nothrow @nogc
	{
		foreach(c; _containers) TypedMem.free(c);
		_containers.clear();
	}


	/+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	 + Methods to extract the exact containers from the collection.
	 +/
	/**
	 * Attemps to find the callback related to the given trigger.
	 */
	private pragma(inline, true) auto locateByTrigger(const char shortTrigger) nothrow @nogc
	{
		return _containers.findElement!("a.shortTrigger == b")(shortTrigger);
	}

	/**
	 * Attemps to find the callback related to the given trigger.
	 */
	private pragma(inline, true) auto locateByTrigger(const string longTrigger) nothrow @nogc
	{
		return _containers.findElement!("a.longTrigger == b")(longTrigger);
	}


	/+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	 + Methods to test the validity of the arguments being added to the internal collection.
	 +/
	/**
	 * Inspects if the given trigger is not the no short trigger magic value.
	 */
	private void enforceTrigger(const char trigger)
	{
		enforce(trigger != ReadOnlyContainer.NO_SHORT_TRIGGER, "Invalid short trigger '" ~
			ReadOnlyContainer.NO_SHORT_TRIGGER ~ "'.");
		enforce(!containsCallback(trigger), "Error: attempting to add an already present trigger: '"
			~ trigger ~ "'!");
		enforce(trigger != shortHelp, "Invalid short trigger '" ~ shortHelp ~ "' while autoHelp is
			enabled!");
	}

	/**
	 * Inspects if the given trigger is not the no long trigger magic value.
	 */
	private void enforceTrigger(const string trigger)
	{
		enforce(trigger != ReadOnlyContainer.NO_LONG_TRIGGER, "Invalid long trigger \"" ~
			ReadOnlyContainer.NO_LONG_TRIGGER ~ "\".");
		enforce(!containsCallback(trigger), "Error: attempting to add an already present trigger: \""
			~ trigger ~ "\"!");
		enforce(trigger != longHelp, "Invalid long trigger \"" ~ longHelp ~ "\" while autoHelp is enabled!");
	}


	/+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	 + Parsing assistance methods.
	 +/
	/**
	 * Returns the deducted type of the given argument.
	 */
	private const(ArgumentType) identifyArgumentType(const string argument)	const nothrow pure @nogc @safe
	{
		if(argument.length > 1)
		{
			version(Windows)
			{
				if(argument[0] == '/')
				{
					if(argument.length > 2)
						return ArgumentType.Long;
					else
						return ArgumentType.Short;
				}
			}

			if(argument[0] == '-')
			{
				if(argument.length > 2 && argument[1] == '-')
					return ArgumentType.Long;
				else
					return ArgumentType.Short;
			}
		}

		return ArgumentType.Option;
	}


	/**
	 * Removes the trigger separator deducting from the type.
	 */
	private const(string) stripTriggerSeparator(string trigger, const ArgumentType type) const nothrow pure @nogc @safe
	{
		version(Windows)
		{
			// Removes an extra separator character if using unix style trigger.
			if(type == ArgumentType.Long && trigger[0] != '/')
				trigger = trigger[1..$];
		}
		else
		{
			// Removes an extra second separator in the unix style trigger.
			if(type == ArgumentType.Long)
				trigger = trigger[1..$];
		}

		// Removes the remaing/first separator.
		return trigger[1..$];
	}


	/**
	 * Invokes the given containers with the given options, returning the options that were not
	 * required by any container.
	 */
	private const(string[]) invokeContainers(Array!(Holder*) holders, string[] triggerOptions) const
	{
		size_t maxCount = 0;

		foreach(h; holders)
		{
			const size_t used = h.container.invokeWith(h.sender, h.senderType, triggerOptions);

			// Re-assigns the maximum number of options used if necessary.
			if(used > maxCount)
				maxCount = used;
		}

		if(maxCount >= triggerOptions.length)
			return new string[0];
		else
			return triggerOptions[maxCount..$];
	}


	private void cleanHolders(Array!(Holder*) foundHolders)
	{
		foreach(h; foundHolders) TypedMem.free(h);
		foundHolders.clear();
	}


	/**
	 * Parses the given arguments array and calls the relative callbacks.
	 * Itinerates the given sequence, categorizing the triggers into options, short triggers and
	 * long triggers. When a callback is found, it is pushed into the callbacks buffer, then all the
	 * options found after it are pushed into the option buffer.
	 * When a new callback is found, the older ones are invoked giving them the buffered options;
	 * once done, both buffers are cleaned, and the cyle restarts.
	 *
	 * Parameters:
	 *		sequence: 			 the sequence to parse.
	 *		firstTriggerOnly: 	 if set to true, only the first trigger found will be invoked, while
	 							 the other will be treated as options. Default to 'false'.
	 *		unknowTriggerAction: sets the action to undertake when an unknow trigger is found during
	 							 the parsing, default to 'None'.
	 * Return: the unused callback trigger's options.
	 */
	public string[] parse(const string[] sequence,
		const UnknowTriggerAction unknowTriggerAction = UnknowTriggerAction.None,
		const bool firstTriggerOnly = false)
	{
		// Whatever a first trigger has been found.
		bool firstTriggerFound = false;
		// The found trigger's options that will be given to the next callback found.
		string[] triggerOptions;
		// Continious serie of short trigger containers to then call when a long trigger is found.
		Array!(Holder*) foundContainers;
		// Options not used or required by any callback will be returned.
		string[] unusedOptions;


		foreach(const string arg; sequence)
		{
			const ArgumentType type = identifyArgumentType(arg);


			// Function nested to easly access the method arguments and arrays.
			void handleTrigger(string trigger, const bool isShort)
			{
				// The found container.
				auto container = locateByTrigger(trigger);

				if(container.isNull)
				{
					if(isShort)
						trigger = "-" ~ trigger;

					final switch(unknowTriggerAction)
					{
						case UnknowTriggerAction.Throw:
							throw new Exception("Unknow " ~ (isShort) ? "long" : "short" ~ " trigger '"
								~ trigger ~ "'.");
						case UnknowTriggerAction.AsUnused:
							unusedOptions ~= trigger;
							break;
						case UnknowTriggerAction.AsOption:
							triggerOptions ~= trigger;
							break;
						case UnknowTriggerAction.None:
							break;
					}
				}
				else
					foundContainers ~= TypedMem.malloc!Holder(trigger, isShort ? ArgumentType.Short
						: ArgumentType.Long , container.get());
			}


			if(type == ArgumentType.Option || (firstTriggerOnly && firstTriggerFound))
				triggerOptions ~= arg;
			else
			{
				// Invokes the previously found callbacks.
				unusedOptions ~= invokeContainers(foundContainers, triggerOptions);
				// Empties the previous containers and trigger arrays.
				triggerOptions.length = 0;
				cleanHolders(foundContainers);

				// At least a trigger has been found.
				firstTriggerFound = true;
				// Removing either "-", "--" or "/".
				const string strippedArg = stripTriggerSeparator(arg, type);


				if(type == ArgumentType.Short)
				{
					/// Short triggers may come chained together. Ex "-aBv"
					foreach(const char trigger; strippedArg)
					{
						if(trigger == shortHelp && _helpCallable != null)
							_helpCallable(to!string(trigger), ArgumentType.Short,
								_containers[].map!("a.description").array().dup);
						else
							handleTrigger(to!string(trigger), true);
					}
				}
				else
				{
					if(strippedArg == longHelp && _helpCallable != null)
						_helpCallable(strippedArg, ArgumentType.Long,
							_containers[].map!("a.description").array().dup);
					else
						handleTrigger(strippedArg, false);
				}
			}
		}

		// Calls the remaining containers.
		unusedOptions ~= invokeContainers(foundContainers, triggerOptions);

		return unusedOptions;
	}
}


// Tests callback compatibility with isCallback!C and addCallback acceptance
unittest
{
	void exemplarCallback(string s, ArgumentType t, string[] o) { }
	auto fn = (string s, ArgumentType at, string[] ar) { };
	auto dg = (string s, ArgumentType at, string[] ar) { };
	struct S { public void opCall(string s, ArgumentType t, string[] o) { } }

	S st;

	ArgParser ap = getParser();
	ap.addCallback(&exemplarCallback, 'e', false);
	ap.addCallback(fn, 'f', false);
	ap.addCallback(dg, 'd', false);
	ap.addCallback(st, 's', false);
	ap.clearCallbacks();
}


unittest
{
	bool hasThrow = false;

	ArgParser ap = getParser();

	try { ap.parse(["Eleonore", "f", "-d", "--300"], UnknowTriggerAction.Throw); }
	catch(Exception e) { hasThrow = true; }

	ap.clearCallbacks();
	assert(hasThrow);
}


// Basic testing
unittest
{
	import std.conv : to;

	int victimA = 0;
	immutable string[2] rightUnused = ["1", "abc"];

	ArgParser parser = getParser();
	DelegateCallback dg = { victimA++; };
	parser.addCallback(dg, 1L, 'a', false);
	parser.addCallback(dg, 1L, "victimA", false);

	string[] unused = parser.parse(["1", "-a", "12345", "--victimA", "option-1", "abc", "--victimB"]);
	parser.clearCallbacks();

	assert(victimA == 2, "VictimA called " ~ to!string(victimA) ~ " out of " ~ to!string(2) ~ " times.");
	assert(unused == rightUnused, "Wrong unused return: " ~ to!string(unused) ~ " instead of " ~
		to!string(rightUnused) ~ ".");
}


// Testing windows argument notation
unittest
{
	version(Windows)
	{
		import std.conv : to;

		int victimWAReq = 2; // @suppress(dscanner.suspicious.unmodified)
		int victimWA = 0;

		ArgParser parser = getParser();
		parser.addCallback(delegate(string s, ArgumentType at, string[] as) { victimWA++; }, 1L, 'a', "victimWA", false);

		string[] unused = parser.parse(
			["1", "/a", "12345", "/d", "/victimWA", "option-1", "abc", "/victimWB"]);
		parser.clearCallbacks();

		assert(victimWA == victimWAReq, "VictimWA called " ~ to!string(victimWA) ~ " out of "
			~ to!string(victimWAReq));
		assert(unused == ["1", "abc"], "Wrong unused return: " ~ to!string(unused));
	}
}


// Testing single trigger parsing capabilities
unittest
{
	import std.conv : to;

	bool victimC = false;
	bool victimD = false;

	ArgParser ap = getParser();
	ap.addCallback((string, ArgumentType, string[]) => victimC = true, 'c', "victimC", false);
	ap.addCallback((string, ArgumentType, string[]) => victimD = true, 'd', "victimD", false);

	auto un = ap.parse(["213", "--victimC", "-d", "12345", "-c", "--victimD"], UnknowTriggerAction.None, true);
	ap.clearCallbacks();

	assert(victimC, "VictimA has NOT been raised. (is set to " ~ to!string(victimC) ~ ")");
	assert(!victimD, "VictimD has been raised. (is set to " ~ to!string(victimD) ~ ")");
	assert(un == ["213"], "Wrong unused return: " ~ to!string(un));
}


// Testing call of multiple short arguments glued together.
unittest
{
	import std.conv : to;

	int victimE = 0;
	int victimF = 0;

	ArgParser parser = getParser();
	parser.addCallback((string, ArgumentType, string[]) { victimE++; }, 1, 'e', "victimE", false);
	parser.addCallback((string, ArgumentType, string[]) { victimF++; }, 1, 'f', "victimF", false);

	parser.parse(["1", "-ef", "12345", "--victimE", "option-1", "abc", "--victimB"]);
	parser.clearCallbacks();

	assert(victimE == 2 && victimF == 1);
}


// Testing async callbacks.
unittest
{
	ArgParser ap = getParser();
	ap.addCallback((string, ArgumentType, string[]) { }, 'a', true);
	ap.parse(["-a", "--beta"]);
	ap.clearCallbacks();
}

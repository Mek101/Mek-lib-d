module meklib.utils.enumeration;
import meklib.utils.traits;
import std.traits;


/**
 * Tests if an enumeration value has a flag.
 */
public pragma(inline, true) bool hasFlag(T)(T value, T flag)
if(is(T == enum))
{
	return (value & flag) == flag;
}


/**
 * Sets a flag into an enumeration value.
 */
public pragma(inline, true) void setFlag(T)(ref T value, T flag)
if(is(T == enum))
{
	value = value | flag;
}


/**
 * Removes a flag into an enumeration value.
 */
public pragma(inline, true) void removeFlag(T)(ref T value, T flag)
if(is(T == enum))
{
	value = value & ~flag;
}

unittest
{
	enum Test { A = 1, B = 2, C = 4 }
	Test t = Test.B;

	assert(!t.hasFlag(Test.A));
	assert(t.hasFlag(Test.B));
	assert(!t.hasFlag(Test.C));

	t.setFlag(Test.A);
	assert(t.hasFlag(Test.A));
	assert(t.hasFlag(Test.B));
	assert(!t.hasFlag(Test.C));

	t.removeFlag(Test.B);
	assert(t.hasFlag(Test.A));
	assert(!t.hasFlag(Test.B));
	assert(!t.hasFlag(Test.C));
}


/**
 * Combines two value of the same enumeration.
 */
public pragma(inline, true) T combineFlags(T)(T first, T second)
if(is(T == enum))
{
	return first | second;
}


unittest
{
	enum Test { A = 1, B = 2, C = 4 }
	Test t = combineFlags(Test.A, Test.B);

	assert(t.hasFlag(Test.A));
	assert(t.hasFlag(Test.B));
	assert(!t.hasFlag(Test.C));
}


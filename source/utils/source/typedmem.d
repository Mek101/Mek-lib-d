module meklib.utils.typedmem;
import meklib.utils.traits;
import std.traits;
import std.conv;
import core.exception;
import core.stdc.stdlib;

/**
 * Used https://dlang.org/deprecate.html#Class%20allocators%20and%20deallocators as a reference.
 */
// The size of the instanced object.
private template instanceSize(T)
{
	// If T is a class, T.sizeof would have returned the GCed pointer size, not the object real size.
	static if(is(T == class))
		enum size_t instanceSize = __traits(classInstanceSize, T);
	else
		enum size_t instanceSize = T.sizeof;
}

public struct TypedMem
{
	private static void* getMemory(size_t size) @trusted
	{
		// Allocates the memory.
		void* mem = core.stdc.stdlib.malloc(size);
		
		// Memory allocation error managing.
		scope(failure)
			core.stdc.stdlib.free(mem);
		if(mem is null)
			throw new OutOfMemoryError();

		return mem;
	}


	/+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	 + Manual memory allocation.
	 +/
	/**
	 * Allocates and initializes a new T instance.
	 */
	public static auto malloc(T)() @trusted
	{
		size_t size = instanceSize!T;
		void* mem = getMemory(size);

		// Classes require a different emplace.
		static if(isReferenceType!T)
			return emplace!T(mem[0..size]);
		else
			return emplace!T(cast(T*)mem);
	}
	
	/**
	 * Allocates and initializes a new T instance.
	 * Inner classes are not supported.
	 */
	public static auto malloc(T, Args...)(auto ref Args args) @trusted
	if(!isNestedClass!T)
	{
		size_t size = instanceSize!T;
		void* mem = getMemory(size);

		// Classes require a different emplace.
		static if(isReferenceType!T)
			return emplace!T(mem[0..size], args);
		else
			return emplace!T(cast(T*)mem, args);
	}

	/**
	 * Allocates and initializes a new T nested class instance. 
	 */
	public static T malloc(T, Out = typeof(T.outer), Args...)(auto ref Out outObj, auto ref Args args) @trusted
	if(isNestedClass!T)
	{
		size_t size = instanceSize!T;
		void* mem = getMemory(size);
		return emplace!T(mem[0..size], outObj, args);
	}

	/+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	 + Manual memory deallocation.
	 +/
	/**
	 * Frees an object.
	 */
	public static void free(T)(T reference) @trusted
	if(isReferenceType!T)
	{
		void* mem = cast(void*)reference;
		scope(exit)
			core.stdc.stdlib.free(mem);

		destroy(reference);
	}

	/// ditto
	public static void free(T)(T* ptr) @trusted
	if(!isReferenceType!T)
	{
		void* mem = cast(void*)ptr;
		scope (exit)
			core.stdc.stdlib.free(mem);

		destroy(ptr);
	}
}

unittest
{
	// Declaring dummy types.
	class ClassNoArgs { }
	class ClassArgs { this(int a) { } }
	class OuterClass
	{
		class ClassInnerNoArgs { }
		class ClassInnerArgs { this(int a) { } }
	}

	struct StructNoArgs { }
	struct StructArgs { this(int a) { } }

	// Allocating.
	ClassNoArgs cna = TypedMem.malloc!ClassNoArgs();
	ClassArgs ca = TypedMem.malloc!ClassArgs(10);

	OuterClass oc = new OuterClass();
	OuterClass.ClassInnerNoArgs cina = TypedMem.malloc!(OuterClass.ClassInnerNoArgs)(oc);
	OuterClass.ClassInnerArgs cia = TypedMem.malloc!(OuterClass.ClassInnerArgs)(oc, 10);

	StructNoArgs* sna = TypedMem.malloc!StructNoArgs();
	StructArgs* sa = TypedMem.malloc!StructArgs(10);

	// Deallocating.
	TypedMem.free(cna);
	TypedMem.free(ca);

	TypedMem.free(cina);
	TypedMem.free(cia);

	TypedMem.free(sna);
	TypedMem.free(sa);
}
module meklib.utils.traits;
import std.traits;
import std.functional;


/**
 * Determines whatever is convinient to itinerate over an array of the given type with a reference
 * or not.
 */
public enum bool isHeavy(T) = !is(T == class) && !is(T == interface) && !isPointer!T && T.sizeof > ptrdiff_t.sizeof;


/**
 * Returns: true if the C is a callable accepting the given argument types.
 */
public enum bool isCallableWith(C, Args...) = is(Parameters!C == Args);


/**
 * Returns: true if the C is a callable returning a value of type T.
 */
public enum bool returns(C, T) = is(ReturnType!C == T);


/**
 * Returns: true if C is a callable accepting Args as arguemnts and returns a value of type R
 */
public enum bool callableDescribes(alias C, R, Args...) = returns!(C, R) && isCallableWith!(C, Args);


/**
 * Tells if the given enumeration has members whose values overlap the respective bit fields, except
 * for the specified exceptions.
 * This can be useful for flag related methods, to check if the enumeration is consistent with any
 * flag, granting that different flags don't interfere with each another.
 *
 * Limitations: works only on numeric enumerations.
 *
 * Returns: true if a enum member bitfield, different from the given exceptions, overlaps with
			another, false otherwise.
 *
 * Examples:
 * --------------------
 * enum TestSingle { A = 1, B = 2, C = A | B }
 * assert(enumOverplas!(TestSingle, C)); // true
 * assert(enumOverplas!(TestSingle)); // false
 *
 * enum TestDouble { A = 1, B = 2, C = A | B, D = C | B }
 * assert(enumOverplas!(TestDouble, C, D)); // true
 * assert(enumOverplas!(TestDouble, C)); // false
 * --------------------
 */
public enum bool enumOverlaps(E, T...) = enumOverlapsImpl!(E, T);

// N.B. Compile time only.
private bool enumOverlapsImpl(E, T...)()
if(is(E == enum) && isNumeric!E)
{
	bool isAny(E examined)
	{
		foreach(m; T)
			if(examined == m)
				return true;
		return false;
	}


	import std.meta : NoDuplicates;

	foreach(m; NoDuplicates!(EnumMembers!E))
	{
		if(!isAny(m))
			foreach(member; NoDuplicates!(EnumMembers!E))
			{
				/*
				 * Corresponding bitfields setted to 1 result 1, therefore making the result
				 * different from 0.
				*/
				if(member != m && !isAny(member) && (member & m) != 0)
					return true;
			}
	}

	return false;
}

// Overlapping without exceptions.
unittest
{
	enum TestOverlap : ubyte { A = 1, B = 2, C = 3 }
	enum TestNotOverlap : ubyte { A = 1, B = 2, C = 4 }

	assert(enumOverlaps!TestOverlap);
	assert(!enumOverlaps!TestNotOverlap);
}


// Overlapping with exceptions.
unittest
{
	enum Test : ubyte { A = 1, B = 2, C = A | B }

	assert(enumOverlaps!Test);
	assert(!enumOverlaps!(Test, Test.C));
	assert(!enumOverlaps!(Test, Test.C, Test.B));
}


/**
 * Aliases itself to the most memory efficient Nullable!T struct for the given type T.
 */
public template OptimalNullable(T)
{
	import std.typecons : Nullable;
	// If an instance of T can be assiged 'null' or T is pointer, it will use the single field struct.
	static if(isReferenceType!T || isPointer!T)
		alias OptimalNullable = Nullable!(T, null);
	else
		alias OptimalNullable = Nullable!(T);
}


public enum bool unaryDescribes(alias pred, A, R) = is(typeof(unaryFun!(pred)(A.init)) == R);
public enum bool binaryDescribes(alias pred, A, B, R) = is(typeof(binaryFun!(pred)(A.init, B.init)) == R);



//public template needOpCallAlias(alias fun)
//{
//    /* Determine whether or not unaryFun and binaryFun need to alias to fun or
//     * fun.opCall. Basically, fun is a function object if fun(...) compiles. We
//     * want is(unaryFun!fun) (resp., is(binaryFun!fun)) to be true if fun is
//     * any function object. There are 4 possible cases:
//     *
//     *  1) fun is the type of a function object with static opCall;
//     *  2) fun is an instance of a function object with static opCall;
//     *  3) fun is the type of a function object with non-static opCall;
//     *  4) fun is an instance of a function object with non-static opCall.
//     *
//     * In case (1), is(unaryFun!fun) should compile, but does not if unaryFun
//     * aliases itself to fun, because typeof(fun) is an error when fun itself
//     * is a type. So it must be aliased to fun.opCall instead. All other cases
//     * should be aliased to fun directly.
//     */
//    static if (is(typeof(fun.opCall) == function))
//    {
//        enum needOpCallAlias = !is(typeof(fun)) && __traits(compiles, () {
//            return fun(Parameters!fun.init);
//        });
//    }
//    else
//        enum needOpCallAlias = false;
//}
//
//import std.meta : AliasSeq;
//
//template varArgFun(alias fun)
//{
//    static if (is(typeof(fun) : string))
//    {
//        /*static if (!fun._ctfeMatchUnary(parmName))
//        {
//            import std.algorithm, std.conv, std.exception, std.math, std.range, std.string;
//            import std.meta, std.traits, std.typecons;
//        }*/
//        auto varArgFun(Args...)(auto ref Args args)
//        {
//            return mixin(fun);
//        }
//    }
//    else static if (needOpCallAlias!fun)
//    {
//        // Issue 9906
//        alias varArgFun = fun.opCall;
//    }
//    else
//    {
//        alias varArgFun = fun;
//    }
//}
//
//
//template callableDescribes_Experimental(alias fun, R, Args...)
//{
//	static if(is(typeof(fun) : string))
//		alias __fun = varArgFun!fun;
//	else
//		alias __fun = fun;
//
//	enum callableDescribes_Experimental = typeof(__fun(Args.init));
//}
//
//unittest
//{
//	//import std.stdio : writeln;
//	//writeln(callableDescribes_Experimental!("a + b == 2", int, int));
//}

enum bool isReferenceType(T) = is(T == class) || is(T == interface);

/**
Copied from the isInnerClass phobos implemetation. Differs in retruning false instead of failing if T is not class
Determines whether `T` is a class nested inside another class
and that `T.outer` is the implicit reference to the outer class
(i.e. `outer` has not been used as a field or method name)

Params:
	T = type to test

Returns:
`true` if `T` is a class nested inside another, with the conditions described above;
`false` otherwise
*/
template isNestedClass(T)
{
	static if (is(T == class) && is(typeof(T.outer)))
	{
		bool hasOuterMember(string[] members...)
		{
			foreach (m; members)
			{
				if (m == "outer")
					return true;
			}
			return false;
		}

		enum bool isNestedClass = __traits(isSame, typeof(T.outer), __traits(parent, T)) &&
										   !hasOuterMember(__traits(allMembers, T));
	}
	else
		enum bool isNestedClass = false;
}

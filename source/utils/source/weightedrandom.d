module meklib.utils.weightedrandom;
import meklib.utils.array.associative;
import meklib.utils.itinerator;
import std.exception;
import std.typecons;
import std.random;
import std.traits;

/**
 * Pool of elements with a weight.
 * Allows to extract a random element from the sequence according to it's weight.
 */
public class WeightedRandom(T, W = float) : RandomicRange!T
if(isNumeric!W)
{
	/+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	 + Fields.
	 +/
	// Couples where each element is associated with it's draw weight.
	private W[T] _pairs;
	// The total sum of all the weights;
	private W _probabilities;
	// Offers a random value. Shared between multiple instances over the same thread.
	private static Random _randomizer;


	public pragma(inline, true) @property size_t 	count() const pure nothrow @safe { return _pairs.length; }
	public pragma(inline, true) @property W probabilities() const pure nothrow @safe @nogc { return _probabilities; }
	

	/+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	 + Constructors.
	 +/
	/// Allocates a new WeightedRandom object instance.
	public this()
	{
		_probabilities = 0;
	}

	// Copy constructor
	private this(W[T] pairs, W probabilities)
	{
		_pairs = pairs;
		_probabilities = probabilities;
	}


	/+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	 + Collection manipulation and inspection methods.
	 +/
	/*
	 * Adds a new element with its weight to the internal collection. 
	 */
	public bool add(T element, const W weight) nothrow pure @safe
	{
		if(_pairs.containsKey(element))
			return false;
		else
		{
			_pairs[element] = weight;
			_probabilities += weight;
			return true;
		}
	}


	/*
	 * Adds a range the internal collection.
	 * Params:
	 *		pairs: 			  the range to add.
	 *		overrideExisting: if set to true, elements from the range already present into the
							  collection will have their weight changed with the range's element,
							  they will be skipped otherwise.
	 */
	public void add(W[T] pairs, const bool overrideExisting)
	{
		if(overrideExisting)
		{
			foreach(T element, W weight; pairs)
			{
				_pairs[element] = weight;
				_probabilities += weight;
			}
		}
		else
		{
			foreach(T element, W weight; pairs)
				if(!_pairs.containsKey(element))
				{
					_pairs[element] = weight;
					_probabilities += weight;
				}
		}
	}


	/**
	 * Params:
	 *		element: the element to search for.
	 *
	 * Returns: the presense of the element in the collection.
	 */
	public pragma(inline, true) bool contains(scope T element) const nothrow pure @safe @nogc
	{
		return _pairs.containsKey(element);
	}


	/*
	 * Removes an element from the collection.
	 */
	public bool remove(scope T element) nothrow @safe
	{
		return _pairs.remove(element);
	}


	/*
	 * Clears the collection.
	 */
	public pragma(inline, true) void clear() nothrow { _pairs.clear(); }


	/+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	 + Weight related methods.
	 +/
	/**
	 * Attemps to extract a random elment from the pool in a weighted manner.
	 * Returns: A pointer to the extracted element, null if it fails.
	 */
	public Nullable!T tryGetRandomElement()
	{
		if(_pairs.length > 0)
		{
			// Advances the random number provvider.
			_randomizer.popFront();
			// Extracts a random value and converts it to the weight type.
			const W extract = cast(W)((_randomizer.front()) % _probabilities);
			// The total weight extracted until now.
			W accumulator = 0;

			foreach(T elem, W value; _pairs)
			{
				accumulator += value;

				if(extract <= accumulator)
					return nullable(elem);
			}
		}

		return Nullable!T.init;
	}


	public void normalizeAt(scope W top) pure @safe
	{
		foreach(T elem, W value; _pairs)		
			_pairs[elem] = cast(W)((value * top) / _probabilities);
		_probabilities = top;	
	}


	/+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	 + Square brakets operator "[]" overloading.
	 +/
	/// Retrives the weight of the given element.
	public pragma(inline, true) W opIndex(scope T element) const nothrow pure @safe { return _pairs[element]; }

	/// Sets the weight of an existing element.
	public void opIndexAssign(W weight, scope T element) nothrow pure @safe
	{
		_probabilities -= _pairs[element];
		_probabilities += weight;
		_pairs[element] = weight;
	}


	/+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	 + RandomicRange interface implementation.
	 +/
	public Nullable!T tryGetNextElement(scope T dummy) { return tryGetRandomElement(); }

	public T getElement()
	{
		enforce(_pairs.length > 0, "No elements to extract.");
		return tryGetRandomElement().get();
	}

	public bool hasElements() const pure nothrow @safe { return _pairs.length != 0; }
}


// Testing instantiation and random value extracting
unittest
{
	import std.conv : to;

	WeightedRandom!char wr = new WeightedRandom!char();
	wr.add('c', 11);
	wr.add('t', 9);

	assert(wr.contains('c'), "Error: could not find element in the collection.");

	 // @suppress(dscanner.suspicious.unmodified)
	Nullable!char support = wr.tryGetRandomElement();
	assert(!support.isNull, "Could not extract a random value.");
	assert(support.get() == 't' || support.get() == 'c');

	wr.clear();
}


unittest
{
	import std.conv : to;

	auto wr = new WeightedRandom!(char, real)();
	wr.add('c', 11);
	wr.add('t', 9);

	wr.normalizeAt(40);
	assert(wr.probabilities == 40);
	assert(wr['t'] == 18, "Value " ~ to!string(wr['t']) ~ " instead of 18.");

	wr.normalizeAt(10);
	assert(wr.probabilities == 10);
	assert(wr['t'] == 4.5, "Value " ~ to!string(wr['t']) ~ " instead of 4,5.");

	wr.clear();
}


unittest
{
	auto wr = new WeightedRandom!(object.Object, ubyte)();
	wr.add(new Object(), 10);
	assert(!wr.tryGetRandomElement().isNull);
}
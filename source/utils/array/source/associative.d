module meklib.utils.array.associative;
import meklib.utils.array;
import meklib.utils.traits;
import std.algorithm.iteration;
import std.functional;
import std.typecons;
import std.traits;
import std.array;
import std.range;


private alias KeyValueTuple = tuple!("key", "value");
/+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 + Finder methods.
 +/
/+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 + Key methods.
 +/
private alias KeyNull(Array) = OptimalNullable!(KeyType!Array);
/**
 * Params:
 *		pred: the method which the array's element and the value had to satisfy.
 *		array: the associative array to search in.
 *		key: the key to compare the array elements with.
 * Returns: A Nullable of the key type with a key if it has been found, or being null otherwise.
 */
public KeyNull!Array findKey(alias pred = "a == b", Array, K)(const Array array, K key)
if(isAssociativeArray!Array && isBinaryPredicate!(pred, KeyType!Array, K))
{
	alias predicate = binaryFun!pred;
	foreach(k; array.keys) if(predicate(k, key)) return KeyNull!Array(k);
	return KeyNull!Array.init;
}


/**
 * Params:
 *		pred: the method which the array's element and the value had to satisfy.
 *		array: the associative array to search in.
 * Returns:  A Nullable of the key type with a key if it has been found, or being null otherwise.
 */
public KeyNull!Array findKey(alias pred, Array)(const Array array)
if(isAssociativeArray!Array && isUnaryPredicate!(pred, KeyType!Array))
{
	alias predicate = unaryFun!pred;
	foreach(k; array.keys) if(predicate(k)) return KeyNull!Array(k);
	return KeyNull!Array.init;
}

unittest
{
	int[char] arr = ['a':1, 'b':2, 'c':3];

	assert(arr.findKey('a') == 'a');
	assert(arr.findKey!("a == 'a'")() == 'a');

	assert(arr.findKey('z').isNull);
	assert(arr.findKey!("a == 'z'")().isNull);
}


public alias indexOf = keyOf;
/**
 * Gets the key which satisfies the predicate.
 * Params:
 *		pred:  the predicate to satisfy.
 *		array: the associative array to search in.
 *		value: the value to compare the array elements with.
 * Retruns:
 *		 A Nullable of the key type with the found key if it has been found, or being null otherwise.
 */
public KeyNull!Array keyOf(alias pred = "a == b", Array, V)(const Array array, V value)
if(isAssociativeArray!Array && isBinaryPredicate!(pred, ValueType!Array, V))
{
	alias predicate = binaryFun!pred;
	foreach(k, v; array) if(predicate(v, value)) return KeyNull!Array(k);
	return KeyNull!Array.init;
}


/**
 * Gets the key which satisfies the predicate.
 * Params:
 *		pred:  the predicate to satisfy.
 *		array: the associative array to search in.
 * Retruns:
 *		A Nullable of the key type with the found key if it has been found, or being null otherwise.
 */
public KeyNull!Array keyOf(alias pred, Array)(const Array array)
if(isAssociativeArray!Array && isUnaryPredicate!(pred, ValueType!Array))
{
	alias predicate = unaryFun!pred;
	foreach(k, v; array) if(predicate(v)) return KeyNull!Array(k);
	return KeyNull!Array.init;
}

unittest
{
	int[char] arr = ['a':1, 'b':2, 'c':3];

	assert(arr.keyOf(1) == 'a');
	assert(arr.keyOf!("a == 1")() == 'a');

	assert(arr.keyOf(10_000).isNull);
	assert(arr.keyOf!("a == 10_000")().isNull);
}



/+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 + Value methods.
 +/
private alias ValueNull(Array) = OptimalNullable!(ValueType!Array);
public alias findElement = findValue;
/**
 * Params:
 *		pred: the method which the array's element and the value had to satisfy.
 *		array: the associative array to search in.
 *		value: the value to compare the array elements with.
 * Returns: A Nullable of the value type with the found value if it has been found, or being null otherwise.
 */
public ValueNull!Array findValue(alias pred = "a == b", Array, V)(const Array array, V value)
if(isAssociativeArray!Array && isBinaryPredicate!(pred, ValueType!Array, V))
{
	alias predicate = binaryFun!pred;
	foreach(v; array) if(predicate(v, value)) return ValueNull!Array(v);
	return ValueNull!Array.init;
}

/**
 * Params:
 *		pred: the method which the value has to satisfy.
 *		array: the associative array to search in.
 * Returns: A Nullable of the value type with the found value if it has been found, or being null otherwise.
 */
public ValueNull!Array findValue(alias pred, Array)(const Array array)
if(isAssociativeArray!Array && isUnaryPredicate!(pred, ValueType!Array))
{
	alias predicate = unaryFun!pred;
	foreach(v; array) if(predicate(v)) return ValueNull!Array(v);
	return ValueNull!Array.init;
}

unittest
{
	int[char] arr = ['a':1, 'b':2, 'c':3];

	assert(arr.findValue(1) == 1);
	assert(arr.findValue!("a == 1")() == 1);

	assert(arr.findValue(10_000).isNull);
	assert(arr.findValue!("a == 10_000")().isNull);
}

// Testing for different symbols to not overlap each other.
unittest
{
	int[char] asArr = ['a':1, 'b':2, 'c':3];

	assert(asArr.findElement(1).get() == 1);
	assert(asArr.findElement!("a == 1")().get() == 1);

	import meklib.utils.array.native : findElement;
	ubyte[] arr = [1, 2, 3, 4, 5];

	assert(arr.findElement(1).get() == 1);
	assert(arr.findElement!("a == 1")().get() == 1);
}



/**
 * Gets the value which satisfies the predicate.
 * Params:
 *		pred:  the predicate to satisfy.
 *		array: the associative array to search in.
 *		key:   the key to compare the array keys with.
 * Retruns:
 *		A Nullable of the value type with the found value if it has been found, or being null otherwise.
 */
public ValueNull!Array valueOf(alias pred = "a == b", Array, K)(const Array array, K key)
if(isAssociativeArray!Array && isBinaryPredicate!(pred, ValueType!Array, K))
{
	alias predicate = binaryFun!pred;
	foreach(k, v; array) if(predicate(k, key)) return ValueNull!Array(v);
	return ValueNull!Array.init;
}

/**
 * Gets the value which satisfies the predicate.
 * Params:
 *		pred:  the predicate to satisfy.
 *		array: the associative array to search in.
 * Retruns:
 *		A Nullable of the value type with the found value if it has been found, or being null otherwise.
 */
public ValueNull!Array valueOf(alias pred, Array)(const Array array)
if(isAssociativeArray!Array && isUnaryPredicate!(pred, ValueType!Array))
{
	alias predicate = unaryFun!pred;
	foreach(k, v; array) if(predicate(k)) return ValueNull!Array(v);
	return ValueNull!Array.init;
}

unittest
{
	int[char] arr = ['a':1, 'b':2, 'c':3];

	assert(arr.valueOf('b') == 2);
	assert(arr.valueOf!("a == 'b'")() == 2);

	assert(arr.valueOf('z').isNull);
	assert(arr.valueOf!("a == 'z'")().isNull);
}


/+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 + Content testing methods.
 +/
/+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 + Inspects by key.
 +/
pragma(inline, true):
/**
 * Params:
 *		pred: the method which the array's keys and the given key has to satisfy.
 *		array: the associative array to search in.
 *		key: the key to search inside the array.
 * Returns:
 *		The presence of the key in the collection.
 */
public bool containsKey(Array, K)(const Array array, K key)
if(isAssociativeArray!Array && is(K : KeyType!Array))
{
	return (key in array) != null;
}

///ditto
public bool containsKey(alias pred, Array, K)(const Array array, K key)
if(isAssociativeArray!Array)
{
	return !array.findKey!(pred)(key).isNull;
}



/+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 + Inspects by value.
 +/
/**
 * Params:
 *		pred: the method which the array's values and the given value has to satisfy.
 *		array: the associative array to search in.
 *		value: the value to search inside the array.
 * Returns:
 *		The presense of the value in the array.
 */
public bool containsValue(alias pred = "a == b", Array, V)(const Array array, V value)
if(isAssociativeArray!Array)
{
	return !array.findValue!(pred)(value).isNull;
}


/+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 + Inspects by key and value.
 +/
/**
 * Params:
 *		pred: the method which the array's key-value pairs and the given key and value has to satisfy.
 			 Must accept a key-value tuple as second argument.
 *		array: the associative array to search in.
 *		key: the key to search inside the array.
 *		value: the value to search inside the array.
 * Returns:
 *		The presense of a pair with both the value and the key in the array.
 */
public bool contains(Array, K, V)(const Array array, K key, V value)
if(isAssociativeArray!Array && is(K : KeyType!Array) && is(V : ValueType!Array))
{
	const ValueType!(Array)* p = key in array;
	return p != null && *p == value;
}

///ditto
public bool contains(alias pred, Array, K, V)(const Array array, K key, V value)
if(isAssociativeArray!Array)
{
	alias predicate = binaryFun!pred;
	foreach(p; array.byPair) if(predicate(p, tuple(key, KeyValueTuple(key, value)))) return true;
	return false;
}

unittest
{
	int[char] arr = ['a':1, 'b':34, '5':5, '6':65, 't':76, 'j':9, ':':0, '\0':1];

	assert(arr.containsKey('b'));
	assert(arr.containsValue(0));
	assert(arr.contains('6', 65));

	assert(!arr.containsKey('z'));
	assert(!arr.containsValue(10_000));
	assert(!arr.contains('z', 10_000));
}



/+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 + Collecting methods.
 +/
/+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 + By key methods.
 +/
public auto filterKey(alias pred, Array)(const Array array)
if(isAssociativeArray!Array && isUnaryPredicate!(pred, KeyType!Array))
{
	return array.keys.filter!(pred);
}

unittest
{
	immutable int[char] arr = ['a':1, 'b':34, '5':5, '6':65, 't':76, 'j':9, ':':0, '\0':1];
	immutable auto res2 = arr.filterKey!("a == 't'");
}


/+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 + By value methods.
 +/
public auto filterValue(alias pred, Array)(const Array array)
if(isAssociativeArray!Array && isUnaryPredicate!(pred, ValueType!Array))
{
	return array.values.filter!(pred);
}

unittest
{
	immutable int[char] arr = ['a':1, 'b':2, 'c':3, 'd':4, 'e':5];
	immutable auto res2 = arr.filterValue!("a == 2");
}



/+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 + Element removal.
 +/
/+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 + Element removal by key.
 +/
/**
 * Remove a key-value pair by the key. Alters the source array.
 * Params:
 *		pred: the method which the array's key and the given key has to satisfy.
 *		array: the associative array to search in.
 *		key: the key to search inside the array.
 * Returns:
 *		If the key-value pair with the given key has been removed.
 */
public bool removeByKey(alias pred, Array, V)(ref Array array, K key)
if(isAssociativeArray!Array && isBinaryPredicate!(pred, KeyType!Array, K))
{
	return removeImpl(array, array.findKey!(pred)(key));
}

/**
 * Remove a key-value pair by the key. Alters the source array.
 * Params:
 *		pred: the method which the array's key has to satisfy.
 *		array: the associative array to search in.
 * Returns:
 *		If the key-value pair has been removed.
 */
public bool removeByKey(alias pred, Array)(ref Array array)
if(isAssociativeArray!Array && isUnaryPredicate!(pred, KeyType!Array))
{
	return removeImpl(array, array.findKey!(pred)());
}


/+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 + Element removal by value.
 +/
/**
 * Remove a key-value pair by the value. Alters the source array.
 * Params:
 *		pred: the method which the array's value and the given value has to satisfy.
 *		array: the associative array to search in.
 *		value: the value to search inside the array.
 * Returns:
 *		If the key-value pair with the given value has been removed.
 */
public bool removeByValue(alias pred, Array, V)(ref Array array, V value)
if(isAssociativeArray!Array && isBinaryPredicate!(pred, ValueType!Array, V))
{
	return removeImpl(array, array.keyOf!(pred)(value));
}

/**
 * Remove a key-value pair by the value. Alters the source array.
 * Params:
 *		pred: the method which the array's value has to satisfy.
 *		array: the associative array to search in.
 * Returns:
 *		If the key-value pair with has been removed.
 */
public bool removeByValue(alias pred, Array, V)(ref Array array)
if(isAssociativeArray!Array && is(isUnaryPredicate!(pred, ValueType!Array)))
{
	return removeImpl(array, array.keyOf!(pred)());
}


private bool removeImpl(Array)(ref Array array, KeyNull!Array k)
{
	return !k.isNull && array.remove(k.get());
}


/+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 + Element removal by value.
 +/
/**
 * Remove a key-value pair by the key and value. Alters the source array.
 * Params:
 *		pred: the method which the array's value and the given value has to satisfy. Must accept a
			  key-value tuple as second argument.
 *		array: the associative array to search in.
 *		key: the key to search inside the array
 *		value: the value to search inside the array.
 * Returns:
 *		If the key-value pair with the given value and given key has been removed.
 */
public bool remove(alias pred = "a == b", Array, K, V)(ref Array array, K key, V value)
if(isAssociativeArray!Array)
{
	alias predicate = binaryFun!pred;
	foreach(pair; array.byPair) if(predicate(pair, KeyValueTuple(key, value))) return array.remove(pair.key);
	return false;
}

/**
 * Remove a key-value pair by the key and value. Alters the source array.
 * Params:
 *		pred: the method which the array's value and the given value had to satisfy.
 *		array: the associative array to search in.
 * Returns:
 *		If the key-value pair has been removed.
 */
public bool remove(alias pred, Array)(ref Array array)
if(isAssociativeArray!Array)
{
	alias predicate = unaryFun!pred;
	foreach(pair; array.byPair) if(predicate(pair)) return array.remove(pair.key);
	return false;
}

unittest
{
	int[char] arr = ['a':0, 'b':1];

	assert(arr.remove!("a == b")('a', 0)); // 'arr.remove('a', 0)' calls the default 'aa.remove('a')' for some reason.
	assert(arr.remove!("a.key == 'b' && a.value == 1")());

	assert(!arr.contains('a', 0));
	assert(!arr.contains('b', 1));
}

module meklib.utils.itinerator;
import meklib.utils.enumeration;
import meklib.utils.traits;
import std.range.interfaces;
import std.exception;
import std.typecons;


/**
 * Specifies how the itinerator should behave.
 */
public enum ItinerationMode : ubyte
{
	/// Elemets keep being generated forever.
	Infinite = 1,
	/// Elements are generated until the chain is unable to extract a following element.
	FiniteRandom = 2,
	/// Generates at least the given number of elements.
	FiniteMin = 4,
	/// Generates up to the given number of elements.
	FiniteMax = 8,
	// Generates a number of elements between the given range.
	FiniteSized = FiniteMin | FiniteMax
}

static assert(!enumOverlaps!(ItinerationMode, ItinerationMode.FiniteSized));


/**
 * Represents a random generated range generator.
 */
public interface RandomicRange(T)
{
	/**
	 * Attemps to extract a element basing off the given element.
	 */
	Nullable!T tryGetNextElement(T element);

	/**
	 * Forcibly extracts a new element.
	 */
	T getElement();

	/**
	 * Determinates if the generator has any elements to extract.
	 */
	bool hasElements() const nothrow pure @nogc;


	/+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	 + Range itineration modes.
	 +/
	pragma(inline, true):
	/**
	 * Gets an itinerator.
	 */
	public final InputRange!T itinerateAs(bool finite)
	{
		return new RandomRangeItinerator!T(this, getElement(), finite ? ItinerationMode.FiniteRandom
			: ItinerationMode.Infinite);
	}

	/// ditto
	public final InputRange!T itinerateMinimum(const size_t lentgh)
	{
		return new RandomRangeItinerator!T(this, getElement(), ItinerationMode.FiniteMin, lentgh);
	}

	/// ditto
	public final InputRange!T itinerateMaximum(const size_t lentgh)
	{
		return new RandomRangeItinerator!T(this, getElement(), ItinerationMode.FiniteMax, 0, lentgh);
	}

	/// ditto
	public final InputRange!T itinerateRange(const size_t min, const size_t max)
	{
		return new RandomRangeItinerator!T(this, getElement(), ItinerationMode.FiniteSized, min, max);
	}

	/**
	 * Gets an itinerator from a starting element.
	 */
	public final InputRange!T itinerateAsFrom(T element, bool finite)
	{
		return new RandomRangeItinerator!T(this, element, finite ? ItinerationMode.FiniteRandom
			: ItinerationMode.Infinite);
	}

	/// ditto
	public final InputRange!T itinerateMinFrom(T element, const size_t lentgh)
	{
		return new RandomRangeItinerator!T(this, element, ItinerationMode.FiniteMin, lentgh);
	}

	/// ditto
	public final InputRange!T itinerateMaxFrom(T element, const size_t lentgh)
	{
		return new RandomRangeItinerator!T(this, element, ItinerationMode.FiniteMax, 0, lentgh);
	}

	/// ditto
	public final InputRange!T itinerateRangeFrom(T element, const size_t min, const size_t max)
	{
		return new RandomRangeItinerator!T(this, element, ItinerationMode.FiniteSized, min, max);
	}
}



/**
 * Single use itinerator over a lazy generated random element range.
 * Laizly extracts a new element while itinerated.
 */
private class RandomRangeItinerator(T) : InputRange!T
{
	// Costant fields.
	private RandomicRange!T _source;
	private ItinerationMode _mode;

	// Lower limit, included
	private size_t _min;
	// Upper limit, not included
	private size_t _max;

	// Variable field.
	public size_t size;

	// Data fields.
	private bool _empty;
	private T _front;


	/**
	 * Lazly generates a range from the given MarkovChain with a least 'min'
	 * elements and within 'max' elements ('max' not included).
	 */
	public this(RandomicRange!T source, const ItinerationMode mode, const size_t min = 0,
		const size_t max = size_t.max)
	{
		this(source, source.getElement(), mode, min, max);
	}


	/// ditto
	public this(RandomicRange!T source, T startElement, const ItinerationMode mode,
		const size_t min = 0, const size_t max = size_t.max) pure @safe
	{
		// The maximum size must be either greater or equal to the minimum size.
		enforce(min <= max, "Minimum size greater than maximum size.");

		_source = source;
		_mode = mode;
		_min = min;
		_max = max;

		_max--; // Decreased to allow the size to reach it and prevent bugs.

		set(startElement);
	}


	/*
	 * Sets the itinerator state, setting a front.
	 */
	private void set(T elem) @safe
	{
		size = 0;
		_empty = false;
		_front = elem;
	}


	/// The current extracted element.
	public T front() nothrow pure @safe @nogc { return _front; }

	/// Gets and sets the front to it's init value.
	public T moveFront() nothrow pure @safe
	{
		T tmp = _front;
		_front = T.init;
		return tmp;
	}

	/// Empty only if the internal pool has satisfied the itineration mode.
	public bool empty() const nothrow pure @nogc { return !_source.hasElements() || _empty; }

	/// Extracts a new front.
	public void popFront()
	{
		if(_mode.hasFlag(ItinerationMode.FiniteMax) && size >= _max)
			// Maximum size has been reached, there is no need to continue.
			_empty = true;
		else
		{
			// Attemps to extract a following element.
			Nullable!T next = _source.tryGetNextElement(front);

			if(next.isNull)
			{
				// If a following element has not been found.
				if(_mode.hasFlag(ItinerationMode.FiniteRandom) || (_mode.hasFlag(
					ItinerationMode.FiniteMin) && size >= _min))
					/*
					 * If the lentgh is either randomly determinated or the minimum size has been reached, there is no
					 * need to continue.
					 */
					_empty = true;
				else
					/*
					 * Either the lentgh is infinite or the minimum size has not been reached so it continues with a
					 * new random extracted element.
					 */
					_front = _source.getElement();
			}
			else
				// The following element has been found and extracted.
				_front = next.get();


			// Finally increases the current range size.
			size++;

			debug(itineration)
			{
				import std.stdio : writefln;
				writefln("Status:\n\tmin: %d\n\tmax: %d\n\tsize: %d\n\tFiniteRandom: %s",
					_min, _max, size, _mode.hasFlag(ItinerationMode.FiniteRandom));
			}
		}
	}


	public int opApply(scope int delegate(T) operation)
	{
		// Why the loop has terminated.
		int status = 0;
		for(; !empty; popFront())
		{
			status = operation(front);
			// Checks if the itineration has to be interrupted.
			if(status != 0)
				break;
		}
		return status;
	}


	public int opApply(scope int delegate(size_t, T) operation)
	{
		// Why the loop has terminated.
		int status = 0;
		for(size_t i = 0; !empty; popFront(), i++)
		{
			status = operation(i, front);
			// Checks if the itineration has to be interrupted.
			if(status != 0)
				break;
		}
		return status;
	}
}



// Testing with WeightedRandom
unittest
{
	import meklib.utils.weightedrandom : WeightedRandom;
	auto wr = new WeightedRandom!int();
	wr.add('c', 10);

	InputRange!int r = wr.itinerateMinimum(1);
}



unittest
{
	import meklib.algorithms.markovchain : MarkovChain;
	auto mc = new MarkovChain!char();
	mc.loadSequence(['a', 'b']);

	InputRange!char r = mc.itinerateAs(true);
}
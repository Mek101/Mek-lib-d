module meklib.utils.array.container;
import meklib.utils.array;
import meklib.utils.traits;
import std.algorithm.searching;
import std.functional;
import std.container;
import std.typecons;
import std.traits;
import std.range;


/+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 + Search elements.
 +/
/**
 * Params:
 *		array: the std.container.Array to search in.
 *		value: the value to search inside the array.
 * Returns: the index of the value inside the array. size_t.max if not found.
 */
public NullSized indexOf(alias pred = "a == b", V, E)(const Array!V array, E value)
if(isBinaryPredicate!(pred, V, E))
{
	alias predicate = binaryFun!pred;
	for(size_t i = 0; i < array.length; i++) if(predicate(array[i], value)) return NullSized(i);
	return NullSized.init;
}


/**
 * Params:
 *		array: the array to search in.
 *		predicate: the method which the value has to satisfy.
 * Returns: the index of the value inside the array. size_t.max if not found.
 */
public NullSized indexOf(alias pred, V)(const Array!V array)
if(isUnaryPredicate!(pred, V))
{
	alias predicate = unaryFun!pred;
	for(size_t i = 0; i < array.length; i++) if(predicate(array[i])) return NullSized(i);
	return NullSized.init;
}


unittest
{
	Object obj = new Object();
	Array!Object arr = [obj, new Object()];

	assert(arr.indexOf(obj).get() == 0);
	assert(arr.indexOf(new Object).isNull);
}


/**
 * Params:
 *		array: the array to search in.
 *		predicate: the method which the value has to satisfy.
 * Returns: a constant pointer to the value which satisfies the predicate, a pointer to null otherwise.
 */
public OptimalNullable!V findElement(alias pred = "a == b", V, E)(Array!V array, E element)
{
	return array[].find!(pred)(element).filtrateRange();
}


/// ditto
public OptimalNullable!V findElement(alias pred, V)(Array!V array)
{
	return array[].find!(pred)().filtrateRange();
}


private OptimalNullable!(ElementType!Range) filtrateRange(Range)(Range range)
if(isInputRange!Range)
{
	alias N = OptimalNullable!(ElementType!Range);
	if(range.length == 0)
		return N.init;
	else
		return N(range[0]);
}


unittest
{
	Array!double arr = [20, 1, 12];


	assert(arr.indexOf(20).get() == 0);
	assert(arr.indexOf(99).isNull);

	assert(arr.indexOf!("a == b")(20).get() == 0);
	assert(arr.indexOf!("a == b")(99).isNull);


	assert(arr.findElement(20).get() == 20);
	assert(arr.findElement(99).isNull);

	assert(arr.findElement!("a == 20")().get() == 20);
	assert(arr.findElement!("a == 99")().isNull);
}


/+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 + Element selection.
 +/
public auto filter(alias pred, V)(ref Array!V array)
{
	import std.algorithm.iteration : filter;
	return array[].filter!(pred);
}

unittest
{
	Array!uint arr = [1, 2, 3, 4];
	arr.filter!((a) => a > 2);
}


/+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 + Test presence.
 +/
/**
 * Params:
 *		array: the std.container.Array to search in.
 *		value: the value to search inside the array.
 * Returns: the presense of the value in the container.
 */
public bool contains(alias pred = "a == b", V, E)(ref Array!V array, E value)
if(isBinaryPredicate!(pred, V, E))
{
	return array[].canFind!(pred)(value);
}

/**
 * Params:
 *		array: the std.container.Array to search in.
 * Returns: the presense of the value in the container.
 */
public bool contains(alias pred, V)(ref Array!V array)
if(isUnaryPredicate!(pred, V))
{
	return array[].canFind!(pred)();
}

unittest
{
	Object obj = new Object();
	Array!Object arr = [obj, new Object()];
	assert(arr.contains(obj));
}


/+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 + Element removal.
 +/
/**
 * Attemps to removes the given element from the Array
 * Params:
 *		pred: the method which the array's value and the given element has to satisfy.
 *		array: the std.container.Array to search in.
 *		element: the element to remove.
 * Returns: if the element was present and then removed.
 */
public bool remove(alias pred = "a == b", V, E)(ref Array!V array, E element)
if(isBinaryPredicate!(pred, V, E))
{
	return removeImpl(array, array.indexOf!(pred)(element));
}


/**
 * Attemps to removes the given element from the Array
 * Params:
 *		pred: the method which the array's value has to satisfy.
 *		array: the std.container.Array to search in.
 * Returns: if the element was present and then removed.
 */
public bool remove(alias pred, V)(ref Array!V array)
if(isUnaryPredicate!(pred, V))
{
	return removeImpl(array, array.indexOf!(pred)());
}


private bool removeImpl(V)(ref Array!V array, NullSized index)
{
	if(index.isNull)
		return false;
	else
	{
		array.removeAt(index.get());
		return true;
	}
}


unittest
{
	Object obj = new Object();
	Array!Object arr = [obj, new Object()];
	assert(arr.remove(obj));
}


unittest
{
	Array!char arr = ['a', 'b', 'c'];
	assert(arr.remove('a'));
	assert(arr.remove!("a == 'b'")());
}



/**
 * Removes the element at the given index.
 * Params:
 *		array: the array to search in.
 *		index: index of the element to remove.
 */
public void removeAt(V)(ref Array!V array, size_t index)
{
	array.linearRemove(array[index..index + 1]);
}

/**
 * Removes the element at the given index.
 * Params:
 *		array: the array to search in.
 *		start: the starting, inclusive index of the elements to remove.
 *		end: the ending, exclusive index of the elements to remove.
 */
public void removeAt(V)(ref Array!V array, size_t start, size_t end)
{
	array.linearRemove(array[start..end]);
}


unittest
{
	Array!int arr1 = [0, 1, 2, 3, 4];
	Array!int arr2 = arr1.dup;

	assert(arr1 == arr2);

	arr1.removeAt(0);	
	assert(arr1 != arr2);
}

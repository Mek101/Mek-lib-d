module meklib.utils.array.native;
import meklib.utils.array;
import meklib.utils.traits;
import std.algorithm.searching;
import std.functional;
import std.typecons;
import std.traits;
import std.range;
import std.array;


/+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 + Index extraction.
 +/
private enum bool isIdexable(R) = isRandomAccessRange!R || isStaticArray!R;
/**
 * Params:
 *		array: the array to search in.
 *		predicate: the method which the range element has to satisfy.
 *		value: the value to confront the range elements with.
 * Returns: the index of the value inside the array. size_t.max if not found.
 */
public NullSized indexOf(alias pred = "a == b", Range, V)(Range array, V value)
if(isIdexable!Range && isBinaryPredicate!(pred, ElementType!Range, V))
{
	alias predicate = binaryFun!pred;
	for(size_t i = 0; i < array.length; i++) if(predicate(array[i], value)) return NullSized(i);
	return NullSized.init;
}


/**
 * Params:
 *		array: the array to search in.
 *		predicate: the method which the value has to satisfy.
 * Returns: the index of the value inside the array. size_t.max if not found.
 */
public NullSized indexOf(alias pred, Range)(Range array)
if(isIdexable!Range && isUnaryPredicate!(pred, ElementType!Range))
{
	alias predicate = unaryFun!pred;
	for(size_t i = 0; i < array.length; i++) if(predicate(array[i])) return NullSized(i);
	return NullSized.init;
}


// Testing for gc dependent code
pure nothrow @safe @nogc unittest
{
	int[7] a = [0, 1, 2, 3, 4, 5, 6];
	assert(a.indexOf(3) == 3);
	assert(a.indexOf!("a == 3")() == 3);
}


// Testing 'indexOf'
unittest
{
	ubyte[] arr = [1, 2, 56, 8, 9, 27, 64, 0];
	ubyte[] arr2;
	for(int i = 0; i < arr.length; i++) arr2 ~= arr[i];

	assert(arr.indexOf(8) == 3);
	assert(arr.indexOf!("a == b")(27) == 5);
	assert(arr2.indexOf!((ubyte a, int b) => a == b)(27) == 5);
}


/+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 + Finder methods.
 +/
/**
 * Params:
 *		range: the range to search in.
 *		predicate: the method which the value has to satisfy.
 * Returns: a constant pointer to the value which satisfies the predicate, a pointer to null otherwise.
 */
public OptimalNullable!(ElementType!Range) findElement(alias pred = "a == b", Range, V)(Range range,
	V element)
if(isInputRange!Range)
{
	return range.find!(pred)(element).filtrateRange();
}


/// ditto
public OptimalNullable!(ElementType!Range) findElement(alias pred, Range)(Range range)
if(isInputRange!Range)
{
	return range.find!(pred)().filtrateRange();
}


private pragma(inline, true) OptimalNullable!(ElementType!Range) filtrateRange(Range)(Range range)
if(isInputRange!Range)
{
	alias N = OptimalNullable!(ElementType!Range);
	if(range.length == 0)
		return N.init;
	else
		return N(range[0]);
}


// Testing 'findElement' with predicate.
unittest
{
	int[] a = [1, 2, 3, 4, 5, 6, 7];
	int*[] b;	

	// Loading into the array of pointers.
	for(int i = 0; i < a.length; i++)
		b ~= &a[i];

	const Nullable!int resA = a.findElement!((const int i) => i == 4)();
	const Nullable!int resA2 = a.findElement!("a == 4")();
	const Nullable!(int*) resB = b.findElement!((const int* i) => *i == 4)();

	assert(!resA.isNull);
	assert(!resA2.isNull);
	assert(!resB.isNull);
}


alias contains = canFind;


/+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 + Element removal.
 +/
/**
 * Attemps to removes the given element from the Range.
 * Params:
 *		pred: the method which the range's value and the given element has to satisfy.
 *		range: the range to search in.
 *		element: the element to remove.
 * Returns: if the element was present and then removed.
 */
public bool remove(alias pred = "a == b", Range, V)(ref Range range, V element)
if(isRandomAccessRange!Range)
{
	return removeImpl(range, range.indexOf!(pred)(element));
}

/**
 * Attemps to removes the given element from the Range.
 * Params:
 *		pred: the method which the range's value has to satisfy.
 *		range: the range to search in.
 * Returns: if the element was present and then removed.
 */
public bool remove(alias pred, Range)(ref Range range)
if(isRandomAccessRange!Range)
{
	return removeImpl(range, range.indexOf!(pred)());
}


private bool removeImpl(Range)(ref Range range, NullSized index)
{
	if(index.isNull)
		return false;
	else
	{
		array.removeAt(index.get());
		return true;
	}
}


/**
 * Removes the element at the given index.
 * Params:
 *		range: the range to search in.
 *		index: index of the element to remove.
 */
public void removeAt(Range)(ref Range range, size_t index)
if(isInputRange!Range)
{
	import std.algorithm : remove;
	range = range.remove(index);
}

/**
 * Removes the element at the given index.
 * Params:
 *		range: the range to search in.
 *		start: the starting, inclusive index of the elements to remove.
 *		end: the ending, exclusive index of the elements to remove.
 */
public void removeAt(Range)(ref Range range, size_t start, size_t end)
if(isInputRange!Range)
{
	import std.algorithm : remove;
	range = range.remove(tuple(start, end));
}

unittest
{
	int[] arr = [0, 1, 2, 3, 4];
	immutable auto arr2 = arr.dup;

	arr.removeAt(1);
	assert(arr != arr2);
}

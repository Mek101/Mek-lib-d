module meklib.utils.array;
import meklib.utils.traits;
import std.typecons;
import std.range;

public import meklib.utils.array.native;
public import meklib.utils.array.container;
public import meklib.utils.array.associative;


package alias NullSized = Nullable!(size_t, size_t.max);
package enum bool isUnaryPredicate(alias pred, A) = unaryDescribes!(pred, A, bool);
package enum bool isBinaryPredicate(alias pred, A, B) = binaryDescribes!(pred, A, B, bool);


//'inunaryPredicate' unittests
unittest
{
	assert(isUnaryPredicate!("a == 2", int));

	// With wrong argument type.
	assert(!isUnaryPredicate!("a == 2", string));
	// With wrong return type.
	assert(!isUnaryPredicate!("'K'", int));
}


//'isBinaryPredicate' unittest
unittest
{
	assert(isBinaryPredicate!("a == b", int, byte));

	// With wrong argument type.
	assert(!isBinaryPredicate!("a + b == 2", string, Object));
	// With wrong return type.
	assert(!isBinaryPredicate!("'K'", int, Object));
}
module meklib.algorithms.dijkstra;
import std.algorithm;
import std.traits;
import std.range;


public struct Arc(N, W)
{
	public N* from;
	public N* to;
	public W weight;

	public this(N* from, N* to, W weight = W.init) pure nothrow @safe @nogc
	{
		this.from = from;
		this.to = to;
		this.weight = weight;
	}
}


unittest
{
	Object o = new Object();
	Object* ptr = null;

	auto a = Arc!(Object, ubyte)(null, null, 10);
	auto b = Arc!(Object, string)(&o, ptr, "a");
}



public class MonodirectionalGraph(N, W = float)
{
	protected N[] _nodes;
	protected Arc!(N, W)[] _arcs;

	/**
	 * Counts.
	 */
	public pragma(inline, true) final size_t nodesCount() const pure nothrow @safe @nogc @property { return _nodes.length; }
	/// ditto
	public pragma(inline, true) final size_t arcsCount()  const pure nothrow @safe @nogc @property { return _arcs.length; }


	/+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	 + Direct manipulation
	 +/
	public @property const(N[])			 nodes() { return _nodes; }
	public @property const(Arc!(N, W)[]) arcs()  { return _arcs; }


	/+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	 + Nodes related methods.
	 +/
	/**
	 * Returns: the inserted node index.
	 */
	public void addNode(N node) nothrow @safe
	{
		_nodes ~= node;
	}

	/**
	 * Removes the node at the given index.
	 */
	public void removeNode(N node)
	{
		_nodes = _nodes.remove!(a => a == node)();
	}


	/**
	 * Number of arcs going to a given node.
	 */
	public size_t nodesCountTo(N node) const
	{
		return _arcs.count!("*a.to == b")(node);
	}

	/**
	 * Number of arcs coming from a given node.
	 */
	public size_t nodesCountFrom(N node) const
	{
		return _arcs.count!("*a.from == b")(node);
	}


	/**
	 * Nodes going to a given node. (Chosen by from).
	 */
	public InputRange!N nodesTowards(N node)
	{
		return _arcs.filter!((a) => *a.from == node).map!("*a.from").inputRangeObject();
	}

	/**
	 * Nodes coming from a given node. (Chosen by to).
	 */
	public InputRange!N nodesComing(N node)
	{
		return _arcs.filter!((a) => *a.from == node).map!("*a.to").inputRangeObject();
	}


	public void clearNodes()
	{
		_nodes.length = 0;
	}


	/+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	 + Arcs related methods.
	 +/
	public void connectNodes(N* nodeFrom, N* nodeTo, W weight)
	{
		_arcs ~= Arc!(N, W)(nodeFrom, nodeTo, weight);
	}

	public void disconnectNodes(N* nodeFrom, N* nodeTo)
	{
		_arcs = _arcs.remove!((e) => e.from == nodeFrom && e.to == nodeTo)();
	}

	public void clearArcs()
	{
		_arcs.length = 0;
	}
}


unittest
{
	auto mg = new MonodirectionalGraph!(char, int)();
	auto mg2 = new MonodirectionalGraph!(Object, Object)();
}



/**
 * Bidirectional Graph
 */
public class BidirectionalGraph(N, W) : MonodirectionalGraph!(N, W)
{
	/**
	 * Nodes going to a given node.
	 */
	public override InputRange!N nodesTowards(N node)
	{
		return _arcs.filter!((a) => *a.to == node || *a.from == node).map!("*a.from").inputRangeObject();
	}

	/**
	 * Nodes coming from a given node. (Chosen by from).
	 */
	public override InputRange!N nodesComing(N node)
	{
		return _arcs.filter!((a) => *a.from == node || *a.to == node).map!("*a.to").inputRangeObject();
	}
}


unittest
{
	auto g = new BidirectionalGraph!(string, int);
	auto g2 = new BidirectionalGraph!(Object, Object);
}
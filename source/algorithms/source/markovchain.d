module meklib.algorithms.markovchain;
import meklib.utils.weightedrandom;
import meklib.utils.itinerator;
import meklib.utils.array;
import std.traits;
import std.exception;
import std.typecons;


public class MarkovChain(E, W = float) : RandomicRange!E
if(isNumeric!W)
{
	private WeightedRandom!(E, W)[E] _associations;

	/+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	 + Methods to load elements.
	 +/
	/*
	 * Counts the elements int the given sequence and how many times they appear.
	 */
	private W[E] countItems(E[] sequence)
	{
		W[E] countedAssociations;

		foreach(elem; sequence)
		{
			if(countedAssociations.containsKey(elem))
				countedAssociations[elem]++;
			else
				countedAssociations[elem] = 1;
		}

		return countedAssociations;
	}

	/**
	 * Loads a sequence of elements.
	 */
	public void loadSequence(E[] sequence)
	{
		if(sequence.length > 0)
		{
			// Itinerates over the given sequence.
			for(size_t i = 0; i < sequence.length; i++)
			{
				// Counts what follows the current new element.
				const W[E] countedAssociations = countItems(sequence[i + 1..sequence.length]);

				// Itinerates over the elements that follow the new element, updating the internal associations.
				foreach(elem, weight; countedAssociations)
				{
					// Writing '_associations[Item][Association]' causes compiler errors.
					WeightedRandom!(E, W) wr;

					if(_associations.containsKey(sequence[i]))
					{
						wr = _associations[sequence[i]];

						if(wr.contains(elem))
							// Updates existing keys. Writing 'wr[Item] += Weight' causes compiler errors.
							wr[elem] = wr[elem] + weight;
						else
							// Creates and initializes new keys.
							wr.add(elem, 1);
					}
					else
					{
						wr = new WeightedRandom!(E, W)();
						wr.add(elem, 1);
						_associations[sequence[i]] = wr;
					}
				}
			}
		}
	}


	/+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	 + Methods to extract elements.
	 +/
	/+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	 + Single element methods.
	 +/
	/**
	 * Tries to extract a following element.
	 */
	public Nullable!E tryGetNextElement(E element)
	{
		if(_associations.containsKey(element))
			return _associations[element].tryGetRandomElement();
		else
			return Nullable!E.init;
	}

	/**
	 * Always extracts a following element.
	 */
	public E getNextElement(E element)
	{
		Nullable!E result = tryGetNextElement(element);

		if(result.isNull)
			return getElement();
		else
			return result.get();
	}

	/**
	 * Always extracts a random element.
	 */
	public E getElement() const @safe
	{
		enforce(_associations.length > 0, "No elements to extract.");
		while(true)
		{
			import std.random : uniform;
			size_t extract = uniform(0, _associations.length);

			foreach(elem; _associations.byKey())
			{
				if(extract == 0)
					return elem;
				else
					extract--;
			}
		}
	}


	/+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	 + Sequence of elements methods.
	 +/
	/**
	 * Generates a sequence of following elements.
	 */
	public pragma(inline, true) E[] getSequence(const size_t minLentgh = 0, const size_t maxLentgh = size_t.max)
	{
		return getSequence(getElement(), minLentgh, maxLentgh);
	}

	/// ditto
	public E[] getSequence(E startElement, const size_t minLentgh = 0, const size_t maxLentgh = size_t.max)
	{
		if(maxLentgh > 0 && _associations.containsKey(startElement))
		{
			E[] newSequence;
			newSequence.reserve(minLentgh);

			foreach(elem; itinerateRangeFrom(startElement, minLentgh, maxLentgh))
				newSequence ~= elem;

			return newSequence;
		}
		else
			return new E[0];
	}


	public bool hasElements() const pure nothrow @safe @nogc { return _associations.length != 0; }
}


unittest
{
	import std.conv : to;
	MarkovChain!char mc = new MarkovChain!char();

	mc.loadSequence(['a', 'b', 'c', 'a', 'a', 't', 'k']);

	size_t count = 0;
	auto itinerator = mc.itinerateRange(3, 7);

	foreach(_; itinerator) count++;
	assert(count > 3 && count <= 7, "Wrong lentgh: " ~ to!string(count) ~ ".");
}


unittest
{
	import std.conv : to;
	MarkovChain!char mc = new MarkovChain!char();

	mc.loadSequence(['g', 'a', 'b', 'a', 't', 'k', 'a', 'U', 'z']);

	char[] arr = mc.getSequence('a', 16, 40);
	assert(arr.length > 16 && arr.length <= 40, "Wrong lentgh: " ~ to!string(arr.length) ~ ".");

	foreach(_; mc.itinerateAs(true)) { }

	//foreach(_; mc.itinerateAs(false)) { }
}


unittest
{
	MarkovChain!Object mc = new MarkovChain!Object();
	mc.loadSequence([new Object(), new Object()]);

	foreach(_; mc.itinerateRange(0, 10)) { }
}
